# SpaceX Dashboard Challenge 
SpaceX , is an American aerospace manufacturer and space transportation services company founded by Elon Musk. 
In this challenge, you need to build a Launch Dashboard for SpaceX.

The unofficial SpaceX API available here. 
Design: 
https://drive.google.com/drive/folders/148JFROLyuqPCLeZ1zzr9k4FZS63xTd4M?usp=sharing 

# Requirements 
- The dashboard should list all launches of SpaceX. 
- The User should be able to view the details of the launch on a modal. 
- The User should be able to filter between All, Upcoming, Successful, and Failed launches. 
- The application should have an empty state and loading state indicator. 
- The design screens do not have designs for mobile screens. We need inputs fromyour end to make the this web application responsive 

# Libraries
- Prime React (UI Library)

# Deployment
 https://spacex-assignment-pathakrahul1.herokuapp.com/

 Note: You can find the deplyed version of this application on above link

 The application is ready for code review.