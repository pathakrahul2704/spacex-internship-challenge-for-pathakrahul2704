import React from "react";
import { Dialog } from "primereact/dialog";
import { classNames } from "primereact/utils";

export default function Modal({ open, onHide, data }) {
  if (!data) return <> </>;
  return (
    <div>
      <Dialog
        // header={header}
        visible={open}
        style={{ width: "50%" }}
        onHide={onHide}
        position="top"
        id="dialog"
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <img
            src={data.og.links.patch.small}
            alt="Logo"
            style={{ height: "70px" }}
          />
          <div style={{ marginLeft: "2rem" }}>
            <div>
              <div style={{ display: "flex" }}>
                <h3 style={{ margin: "5px" }}>{data.mission}</h3>
                <span
                  style={{ marginLeft: "1.5rem" }}
                  className={classNames(
                    "customer-badge",
                    "status-" + data.status
                  )}
                >
                  {data.status}
                </span>
              </div>
              <p style={{ margin: "5px" }}>{data.rocket}</p>
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around",
                width: "80px",
              }}
            >
              <a
                href={data.og.links.article}
                target="_blank"
                rel="noreferrer"
                style={{ textDecoration: "none", color: "#000000" }}
              >
                <i className="pi">A</i>
              </a>
              <a
                href={data.og.links.wikipedia}
                target="_blank"
                rel="noreferrer"
                style={{ textDecoration: "none", color: "#000000" }}
              >
                <i>W</i>
              </a>
              <a
                href={data.og.links.webcast}
                target="_blank"
                rel="noreferrer"
                style={{ textDecoration: "none", color: "#000000" }}
              >
                <i className="pi pi-play"></i>
              </a>
            </div>
          </div>
        </div>
        <p>
          {data.og.rocket.description}
          <a
            href={data.og.rocket.wikipedia}
            target="_blank"
            rel="noreferrer"
            style={{ textDecoration: "none" }}
          >
            Wikipedia
          </a>
        </p>
        <ul style={{ listStyle: "none", padding: "0" }}>
          <li className="dialog-list-item">
            <p>Flight Number</p>
            <p>{data.og.flight_number}</p>
          </li>
          <li className="dialog-list-item">
            <p>Mission Name</p>
            <p>{data.mission}</p>
          </li>
          <li className="dialog-list-item">
            <p>Rocket Type</p>
            <p>{data.og.rocket.type}</p>
          </li>
          <li className="dialog-list-item">
            <p>Manufacturer</p>
            <p>{data.og.rocket.company}</p>
          </li>
          <li className="dialog-list-item">
            <p>Nationality</p>
            <p>{data.og.rocket.country}</p>
          </li>
          <li className="dialog-list-item">
            <p>Launched Date</p>
            <p>{data.date_utc}</p>
          </li>
          <li className="dialog-list-item">
            <p>Payload Type</p>
            <p>{data.og.payloads[0]?.type}</p>
          </li>
          <li className="dialog-list-item">
            <p>Orbit</p>
            <p>{data.orbit}</p>
          </li>
          <li className="dialog-list-item">
            <p>Launch Site</p>
            <p>{data.location}</p>
          </li>
        </ul>
      </Dialog>
    </div>
  );
}
