import { useState } from "react";
import Table from "./Table";
import "./App.css";
import OverlayFilterPanel from "./OverlayFilter";
import Logo from "./assets/logo.png";
import { Divider } from "primereact/divider";

function App() {
  const [filter, setFilter] = useState(null);

  return (
    <>
      <div className="header">
        <img src={Logo} alt="Logo" />
      </div>
      <Divider style={{ margin: "0", padding: "0" }} />
      <div className="App">
        <div className="float-right">
          <OverlayFilterPanel setFilter={setFilter} />
        </div>
        <Table filter={filter} />
      </div>
    </>
  );
}

export default App;
