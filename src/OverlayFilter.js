import React, { useState, useEffect, useRef } from "react";
import { OverlayPanel } from "primereact/overlaypanel";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";

const OverlayFilterPanel = ({ setFilter }) => {
  const [selectedFilter, setSelectedFilter] = useState(null);
  const op = useRef(null);
  const toast = useRef(null);

  useEffect(() => {
    if (selectedFilter) {
      op.current.hide();
      toast.current.show({
        severity: "info",
        summary: "Filter Selected",
        detail: selectedFilter,
        life: 3000,
      });
      setFilter(selectedFilter);
    }
  }, [selectedFilter]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Toast id="toastMsg" ref={toast} position="top-center" />

      <Button
        type="button"
        icon="pi pi-filter"
        label={selectedFilter ? selectedFilter : "All Launches"}
        onClick={(e) => op.current.toggle(e)}
        aria-haspopup
        aria-controls="overlay_panel"
        className="select-product-button"
      />

      <OverlayPanel
        ref={op}
        showCloseIcon
        id="overlay_panel"
        style={{ width: "200px", padding: "0", margin: "0" }}
        className="overlaypanel-demo"
      >
        <ul className="filter-list">
          <li onClick={() => setSelectedFilter("All Launches")}>
            <p>All Launches</p>
          </li>
          <li onClick={() => setSelectedFilter("Upcoming Launches")}>
            <p>Upcoming Launches</p>
          </li>
          <li onClick={() => setSelectedFilter("Successful Launches")}>
            <p>Successful Launches</p>
          </li>
          <li onClick={() => setSelectedFilter("Failed Launches")}>
            <p>Failed Launches</p>
          </li>
        </ul>
      </OverlayPanel>
    </div>
  );
};

export default OverlayFilterPanel;
