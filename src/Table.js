/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { classNames } from "primereact/utils";
import Modal from "./Modal";

const Table = ({ filter }) => {
  const [lunches, setLunches] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [open, setOpen] = useState(false);
  const [modalData, setModalData] = useState(null);

  useEffect(() => {
    fetchCall("https://api.spacexdata.com/v4/launches/query");
  }, [filter]);

  const fetchCall = (uri) => {
    setLoading(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let query = {};
    switch (filter) {
      case "Upcoming Launches":
        query = {
          upcoming: "true",
        };
        break;
      case "Successful Launches":
        query = {
          success: "true",
        };
        break;
      case "Failed Launches":
        query = {
          upcoming: "false",
          success: "false",
        };
        break;
      default:
        query = {};
    }

    var raw = JSON.stringify({
      query,
      options: {
        populate: [
          { path: "payloads" },
          { path: "rocket" },
          { path: "launchpad" },
        ],
        limit: 1000,
      },
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(uri, requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        prepareDateset(data.docs);
      })
      .catch((error) => {
        console.error(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };

  function getClockTime(now) {
    var hour = now.getHours();
    var minute = now.getMinutes();
    if (hour < 10) {
      hour = "0" + hour;
    }
    if (minute < 10) {
      minute = "0" + minute;
    }
    var timeString = hour + ":" + minute;
    return timeString;
  }

  const prepareDateset = (data) => {
    let result = data.map((item, index) => {
      let date = new Date(item.date_utc);
      let utc_date = `${date.getDate()} ${date.toLocaleString("default", {
        month: "long",
      })} ${date.getFullYear()} at ${getClockTime(date)}`;
      return {
        no: index + 1,
        date_utc: utc_date,
        location: item.launchpad.name,
        mission: item.name,
        orbit: item.payloads[0].name,
        status: getStatus(item.upcoming, item.success, item.failure),
        rocket: item.rocket.name,
        og: { ...item },
      };
    });
    setLunches(result);
  };

  const getStatus = (upcoming, success, failure) => {
    if (upcoming) return "Upcoming";
    if (success) return "Success";
    return "Failure";
  };

  if (loading) return "Loading...";
  if (error) return "Error!";

  const statusBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Status</span>
        <span
          className={classNames("customer-badge", "status-" + rowData.status)}
        >
          {rowData.status}
        </span>
      </React.Fragment>
    );
  };

  const noBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">No:</span>
        {rowData.no}
      </React.Fragment>
    );
  };

  const launchBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Launched (UTC)</span>
        {rowData.date_utc}
      </React.Fragment>
    );
  };

  const locationBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Location</span>
        {rowData.location}
      </React.Fragment>
    );
  };

  const missionBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Mission</span>
        {rowData.mission}
      </React.Fragment>
    );
  };

  const orbitBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Orbit</span>
        {rowData.orbit}
      </React.Fragment>
    );
  };

  const rocketBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <span className="p-column-title">Rocket</span>
        {rowData.rocket}
      </React.Fragment>
    );
  };

  const onRowClick = (rowData, e) => {
    setModalData(rowData.data);
    setOpen(true);
  };

  const onHide = () => {
    setOpen(false);
    setModalData(null);
  };

  return (
    <>
      <div className="datatable-responsive-demo">
        <div className="card">
          <DataTable
            value={lunches}
            className="p-datatable-responsive-demo"
            paginator
            rows={10}
            onRowClick={onRowClick}
          >
            <Column field="no" header="No:" body={noBodyTemplate} />
            <Column
              field="date_utc"
              header="Lunched (UTC)"
              body={launchBodyTemplate}
            />
            <Column
              field="location"
              header="Location"
              body={locationBodyTemplate}
            />
            <Column
              field="mission"
              header="Mission"
              body={missionBodyTemplate}
            />
            <Column field="orbit" header="Orbit" body={orbitBodyTemplate} />
            <Column field="status" header="Status" body={statusBodyTemplate} />
            <Column field="rocket" header="Rocket" body={rocketBodyTemplate} />
          </DataTable>
        </div>
      </div>
      <Modal open={open} onHide={onHide} data={modalData} />
    </>
  );
};

export default Table;
